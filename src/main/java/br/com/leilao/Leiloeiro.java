package br.com.leilao;

public class Leiloeiro {

    private String nome;
    private Leilao leilao;

    public Leiloeiro() {

    }

    public Leiloeiro(String nome, Leilao leilao) {
        this.nome = nome;
        this.leilao = leilao;
    }

    public Lance retornarMaiorLance(Leilao leilao) {
       Lance lanceMaior = leilao.getListaLances().get(0);
            for(int i = 1; i < leilao.getListaLances().size(); i++){
                if (leilao.getListaLances().get(i).getValorLance() > lanceMaior.getValorLance()){
                    lanceMaior = leilao.getListaLances().get(i);
                }
            }
       return lanceMaior;
    }
}
