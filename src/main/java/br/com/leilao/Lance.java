package br.com.leilao;

public class Lance {

    private Usuario usuario;
    private Double valorLance;


    public Lance(Usuario usuario, Double valorLance) {
        this.usuario = usuario;
        this.valorLance = valorLance;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Double getValorLance() {
        return valorLance;
    }

    public void setValorLance(Double valorLance) {
        this.valorLance = valorLance;
    }
}
