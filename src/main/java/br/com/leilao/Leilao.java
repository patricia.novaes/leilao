package br.com.leilao;

import java.util.ArrayList;
import java.util.List;

public class Leilao {

    private List<Lance> listaLances;
    private Integer idLeilao;

    public Leilao() {

    }


    public Leilao(List<Lance> listaLances, Integer idLeilao) {
        this.listaLances = listaLances;
        this.idLeilao = idLeilao;
    }




    public Leilao adicionarNovoLeilao(Integer idLeilao) {
        Leilao leilao = new Leilao();
        leilao.idLeilao = idLeilao;
        return leilao;
    }


    public Leilao adicionarNovoLance(Leilao leilao, List<Lance> lances) {
        for (int i = 0; i < lances.size(); i ++){
            if(lances.size() >= 2 && lances.get(i).getValorLance() < lances.get(0).getValorLance()){
               throw new RuntimeException("Atenção valor do lance menor do que o lance anterior");
            }else{
                leilao.listaLances = lances;
            }
        }

        return leilao;
    }

    public List<Lance> getListaLances() {
        return listaLances;
    }

    public void setListaLances(List<Lance> listaLances) {
        this.listaLances = listaLances;
    }

    public Integer getIdLeilao() {
        return idLeilao;
    }

    public void setIdLeilao(Integer idLeilao) {
        this.idLeilao = idLeilao;
    }


}
