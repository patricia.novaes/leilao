package br.com.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class LeilaoTeste {

    private Leilao leilao;
    private List<Lance> lancesTeste;

    @BeforeEach
    public void setUp(){
        leilao = new Leilao();
        lancesTeste = new ArrayList<>();
    }


    @Test
    public void testeCriacaoLeilao(){
        leilao = leilao.adicionarNovoLeilao(11);
        Assertions.assertEquals(11, leilao.getIdLeilao());
    }

    @Test
    public void testeAddNovoLance(){
        Usuario usuario = new Usuario("Patricia Novaes", 1);
        Lance lance = new Lance(usuario, 10.000 );
        lancesTeste.add(lance);
        leilao.adicionarNovoLance(leilao, lancesTeste);
        Assertions.assertNotNull(leilao.getListaLances());
    }

    @Test
    public void testeAddLanceValorMaiorQueAnterior(){
        Usuario usuario = new Usuario("Patricia", 2);
        Lance lance1 = new Lance(usuario, 10.000 );
        Lance lance2 = new Lance(usuario, 5.000);
        lancesTeste.add(lance1);
        lancesTeste.add(lance2);
        Assertions.assertThrows(RuntimeException.class, () -> {leilao.adicionarNovoLance(leilao, lancesTeste);});

    }
}
