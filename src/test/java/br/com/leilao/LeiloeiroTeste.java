package br.com.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class LeiloeiroTeste {

    private Leiloeiro leiloeiro;

    @BeforeEach
    public void setUp(){
        leiloeiro = new Leiloeiro();
    }

    @Test
    public void testeRetornarMaiorLanceDonoLance(){
        Usuario usuarioPerdedor = new Usuario("Joao", 1);
        Usuario usuarioGanhador = new Usuario("Maria", 2);
        Lance lancePerdedor = new Lance(usuarioPerdedor, 25.0);
        Lance lanceGanhador = new Lance(usuarioGanhador, 70.0);
        List<Lance> listaLances = new ArrayList<>();
        listaLances.add(lanceGanhador);
        listaLances.add(lancePerdedor);
        Leilao leilao = new Leilao(listaLances, 25);
        Lance lanceVencedor = leiloeiro.retornarMaiorLance(leilao);
        Assertions.assertEquals(usuarioGanhador.getNome(), lanceVencedor.getUsuario().getNome());
        Assertions.assertEquals(lanceGanhador.getValorLance(), lanceVencedor.getValorLance());
    }


}
