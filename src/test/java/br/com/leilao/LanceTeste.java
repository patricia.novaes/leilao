package br.com.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class LanceTeste {

    @Test
    public void testeCriacaoLance(){
        Usuario usuario = new Usuario("Teste", 123);
        Lance retornoLance = new Lance(usuario, 7.000);
        Assertions.assertNotNull(retornoLance);
    }

}
